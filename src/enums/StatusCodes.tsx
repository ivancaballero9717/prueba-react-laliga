export enum StatusCodes{
    BAD_REQUEST = 400,
    OK = 200,
    SERVER_ERROR = 500,
}
