import { FAILURE_LOGIN, LOADING_LOGIN, LOGOUT, START_LOGIN, SUCCESS_LOGIN } from "../types/auth";
import { LoginBody } from "../../interfaces/acctions/login";

export const startLogin = (payload: LoginBody) => ({
    type: START_LOGIN,
    payload
})

export const logout = () => ({
    type: LOGOUT
})

export const successLogin = (payload: { payload: { token: string } }) => ({
    type: SUCCESS_LOGIN,
    payload
})

export const loadingLogin = (payload: boolean) => ({
    type: LOADING_LOGIN,
    payload
})

export const failureLogin = (payload: string) => ({
    type: FAILURE_LOGIN,
    payload
})
