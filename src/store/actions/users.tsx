import { FAILURE_GET_USERS, LOADING_USERS, START_GET_USERS, SUCCESS_GET_USERS } from "../types/users";
import { UserResponse } from "../../interfaces/UserResponse";

export const startGetUsers = (payload: number) => ({
    type: START_GET_USERS,
    payload
})

export const successGetUser = (payload: UserResponse) => ({
    type: SUCCESS_GET_USERS,
    payload
})

export const loadingUsers = (payload: boolean) => ({
    type: LOADING_USERS,
    payload
})

export const failureGetUsers = (payload: string) => ({
    type: FAILURE_GET_USERS,
    payload
})
