import { all } from "redux-saga/effects";
import auth from "./sagas/auth";
import users from "./sagas/users";

export default function* rootSaga() {
    yield all([
        auth(),
        users()
    ]);
}
