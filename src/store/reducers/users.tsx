import { UsersState } from "../../interfaces/reduxStates/users";
import { FAILURE_GET_USERS, LOADING_USERS, SUCCESS_GET_USERS } from "../types/users";

const initialState: UsersState = {
    data: {
        page: 0,
        per_page: 0,
        total: 0,
        total_pages: 0,
        data: []
    },
    errorMessage: "",
    loading: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SUCCESS_GET_USERS:
            return {
                ...state,
                data: action.payload,
                errorMessage: ""
            }
        case FAILURE_GET_USERS:
            return {
                ...state,
                errorMessage: action.payload
            }
        case LOADING_USERS:
            return {
                ...state,
                loading: action.payload
            }
        default:
            return state
    }
}
