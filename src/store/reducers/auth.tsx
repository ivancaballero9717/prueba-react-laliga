import { FAILURE_LOGIN, LOADING_LOGIN, LOGOUT, SUCCESS_LOGIN } from "../types/auth";
import { AuthState } from "../../interfaces/reduxStates/auth";

const initialState: AuthState = {
    token: "",
    errorMessage: "",
    loading: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SUCCESS_LOGIN:
            return {
                ...state,
                token: action.payload.token,
                errorMessage: ""

            }
        case FAILURE_LOGIN:
            return {
                ...state,
                errorMessage: action.payload

            }
        case LOADING_LOGIN:
            return {
                ...state,
                loading: action.payload
            }
        case LOGOUT:
            return {
                ...state,
                token: ""
            }
        default:
            return state
    }
}
