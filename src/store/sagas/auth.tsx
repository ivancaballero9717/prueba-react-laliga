import { put, call, takeLatest } from "redux-saga/effects"
import { START_LOGIN } from "../types/auth";
import { loginCall } from "../../repositories/authRepository";
import { failureLogin, loadingLogin, successLogin } from "../actions/auth";
import { HttpException } from "../../exceptions/HttpException";
import { LoginAction } from "../../interfaces/acctions/login";

function* login(action: LoginAction) {
    try {
        yield put(loadingLogin(true));
        const result = yield call(loginCall, action.payload);
        yield put(successLogin(result))
    } catch (exception) {
        let message = exception;
        if (exception instanceof HttpException) {
            message = exception.getMessage();
        }
        yield put(failureLogin(message))
    }

    yield put(loadingLogin(false));
}

export default function* auth() {
    yield takeLatest(START_LOGIN, login)
}
