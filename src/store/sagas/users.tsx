import { put, call, takeLatest } from "redux-saga/effects"
import { failureGetUsers, loadingUsers, successGetUser } from "../actions/users";
import { START_GET_USERS } from "../types/users";
import { GetUsersCall } from "../../repositories/usersRepository";
import { HttpException } from "../../exceptions/HttpException";
import { GetUsersAction } from "../../interfaces/acctions/getUsers";


function* getUsers(action: GetUsersAction) {
    try {
        yield put(loadingUsers(true))
        const result = yield call(GetUsersCall, action.payload);
        yield put(successGetUser(result))
    } catch (exception) {
        let message = exception;
        if (exception instanceof HttpException) {
            message = exception.getMessage();
        }
        yield put(failureGetUsers(message))
    }

    yield put(loadingUsers(false))
}

export default function* users() {
    yield takeLatest(START_GET_USERS, getUsers)
}
