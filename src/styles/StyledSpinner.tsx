import styled from "styled-components";

export const StyledSpinner = styled.img`
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 3em;
  height: 3em;
`
