import styled from "styled-components";
import { ColorVariables } from "./ColorVariables";

interface StyledPagination {
    active: boolean
}

export const StyledPaginationItem = styled.li`
  display: inline-block;
  margin: 0 2px;
  width: 44px;
  height: 44px;
  line-height: 42px;
  border: 2px solid ${ColorVariables.grey};
  font-size: 15px;
  font-weight: 700;
  color: ${(props: StyledPagination) => props.active ? ColorVariables.white : ColorVariables.black};
  transition: all .3s;
  background-color: ${(props: StyledPagination) => props.active ? ColorVariables.red : ColorVariables.white};

  &:hover {
    background-color: ${ColorVariables.red};
    border-color: ${ColorVariables.red};
    color: ${ColorVariables.white};
    cursor: pointer;
  }
`;

export const PaginationUlStyled = styled.ul`
  margin-top: 30px;
  text-align: center;
  list-style: none;
`;
