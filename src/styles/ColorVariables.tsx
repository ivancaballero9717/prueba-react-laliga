export const ColorVariables = {
    red: "#f33f3f",
    grey: "#eee",
    white: "#ffffff",
    black: "black",
    greyText: "#BBBBBB",
    blue: "#22C1C3",
    pink: "#F67280",
    saturedPink: "#f45a6a"
}
