import styled from "styled-components";
import { ColorVariables } from "./ColorVariables";

export const StyledNavbar = styled.nav`
  background: ${ColorVariables.pink};
  height: 2rem;
`

export const StyledListContainer = styled.ul`
  list-style-type: none;
  display: flex;
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
;
`

export const StyledListItem = styled.li`
  list-style-type: none;
  margin: 0 auto;
  display: flex;
  width: 100%;
  color: ${ColorVariables.white};
  justify-content: center;
  align-items: center;
  transition: background-color 0.25s ease-out;

  &:hover{
    background-color: ${ColorVariables.saturedPink}
  }
`
