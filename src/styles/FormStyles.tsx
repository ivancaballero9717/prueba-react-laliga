import styled from "styled-components";
import { ColorVariables } from "./ColorVariables";

export const StyledFormContainer = styled.div`
  max-width: 500px;
  min-width: 300px;
  max-height: 700px;
  width: 30%;
  height: 60%;
  margin: 100px auto;
  border-radius: 25px;
`;

export const StyledFormTitle = styled.div`
  text-align: center;
  padding: 2rem 0;
  margin: 0;
  font-size: 2rem;
`;

export const FlexRowForm = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 2rem;
  max-width: 100%;
`;

export const StyledInputForm = styled.input`
  width: 80%;
  box-sizing: border-box;
  border: none;
  border-bottom: 3px solid ${ColorVariables.grey};
  font-size: 1.3rem;
  padding-left: 1.5rem;
  padding-bottom: 1rem;
  transition: border-bottom-color 0.2s ease-in;

  &:focus {
    border-bottom-color: ${ColorVariables.black};
    outline: none;
  }
`;

export const StyledLabelForm = styled.label`
  align-self: start;
  padding-left: 4.5rem;
  padding-bottom: 0.5rem;
  color: ${ColorVariables.greyText}
`;

export const StyledButtonForm = styled.button`
  border-radius: 25px;
  width: 80%;
  height: 40px;
  font-size: 1.3rem;
  color: white;
  font-weight: 700;
  background-color: ${ColorVariables.blue};
  border: 0;
  cursor: pointer;
  transition: opacity 0.25s ease-out;

  &:hover {
    opacity: 0.8;
  }
`;

export const ErrorMessage = styled.p`
  background-color: ${ColorVariables.red};
  text-align: center;
  color: white;
  font-weight: bold;
  border-radius: 5px;
  padding: .5rem;
`;


