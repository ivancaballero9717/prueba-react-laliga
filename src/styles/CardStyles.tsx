import styled from "styled-components";

export const StyledCardContanier = styled.div`
  display: grid;
  gap: 1em;
  grid-template-columns: repeat(auto-fit, minmax(min(100%, 25rem), 1fr));
`

export const StyledCard = styled.div`
  background-color: white;
  border-radius: 0.25rem;
  box-shadow: 0 20px 40px -14px rgba(0, 0, 0, 0.25);
`;

export const StyledContent = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  gap: 1rem;
`

export const StyledTitle = styled.div`
  color: #696969;
  font-size: 1.25rem;
  font-weight: 300;
  letter-spacing: 2px;
  text-transform: uppercase;
`
export const StyledText = styled.p`
  font-size: 0.875rem;
  line-height: 1.5;
`

export const StyledImage = styled.img``;
