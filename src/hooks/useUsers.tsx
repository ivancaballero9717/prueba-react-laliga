import { useSelector } from "react-redux";
import { RootState } from "../store";
import { UsersState } from "../interfaces/reduxStates/users";

export const useUsers = (): UsersState => useSelector((state: RootState) => state.users)
