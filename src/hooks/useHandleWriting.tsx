export const useHandleWriting = (state, setState) => event => {
    setState({
        ...state,
        [event.target.name]: event.target.value
    })
}
