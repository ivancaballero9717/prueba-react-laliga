import { useSelector } from "react-redux";
import { RootState } from "../store";
import { AuthState } from "../interfaces/reduxStates/auth";

export const useAuth = (): AuthState => useSelector((state: RootState) => state.auth)
