export class HttpException {
    message = ""

    constructor(exception) {
        this.message = exception
    }

    getMessage() {
        return this.message
    }
}
