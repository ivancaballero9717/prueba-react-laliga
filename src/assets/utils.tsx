import { StatusCodes } from "../enums/StatusCodes";
import { HttpException } from "../exceptions/HttpException";

export const checkStatusCode = response => {
    switch (response.status) {
        case StatusCodes.BAD_REQUEST:
            throw new HttpException("Invalid credentials");
        case StatusCodes.SERVER_ERROR:
            throw new HttpException("Server error");
        default:
            break;
    }
}



