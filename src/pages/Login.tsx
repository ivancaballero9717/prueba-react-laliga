import React, { useEffect, useState } from "react";
import { useHandleWriting } from "../hooks/useHandleWriting";
import {
    ErrorMessage,
    FlexRowForm,
    StyledButtonForm,
    StyledFormContainer, StyledFormTitle,
    StyledInputForm,
    StyledLabelForm
} from "../styles/FormStyles";
import { useDispatch } from "react-redux";
import { startLogin } from "../store/actions/auth";
import { useAuth } from "../hooks/useAuth";
import { useHistory } from "react-router-dom";
import { Loading } from "../components/Loading";
import { LoginBody } from "../interfaces/acctions/login";


export const Login = () => {
    const [state, setState] = useState<LoginBody>({ email: "", password: "" });
    const history = useHistory();

    const { token, errorMessage, loading } = useAuth();
    const handleChange = useHandleWriting(state, setState);
    const dispatch = useDispatch();

    useEffect(() => {
        token && history.push("/main")
    }, [history, token])

    if (loading) {
        return <Loading/>
    }

    return (
        <div>
            <StyledFormTitle>Login</StyledFormTitle>
            <StyledFormContainer>
                <FormInput
                    onChange={handleChange}
                    name={"email"}
                    label="Username"
                    placeholder="Enter your username"
                    type="text"
                />
                <FormInput
                    onChange={handleChange}
                    name={"password"}
                    label="Password"
                    placeholder="Enter your password"
                    type="password"
                />
                {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
                <FormButton
                    onChange={handleChange}
                    title="Log in"
                    onClick={() => dispatch(startLogin(state))}
                />
            </StyledFormContainer>
        </div>
    )

}

const FormButton = props => (
    <FlexRowForm>
        <StyledButtonForm onClick={props.onClick}>{props.title}</StyledButtonForm>
    </FlexRowForm>
);

const FormInput = props => (
    <FlexRowForm>
        <StyledLabelForm>{props.label}</StyledLabelForm>
        <StyledInputForm name={props.name} onChange={props.onChange} type={props.type} placeholder={props.placeholder}/>
    </FlexRowForm>
);



