import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { startGetUsers } from "../store/actions/users";
import { useUsers } from "../hooks/useUsers";
import { Card } from "../components/Card";
import { CardContainer } from "../components/CardContainer";
import { Pagination } from "../components/Pagination";
import { ErrorMessage } from "../styles/FormStyles";
import { Loading } from "../components/Loading";
import { Navbar } from "../components/Navbar";


export const Main = () => {
    const dispatch = useDispatch();
    const [page, setPage] = useState(1)
    const { data, errorMessage, loading } = useUsers();

    const getUsers = () => {
        dispatch(startGetUsers(page));
    }

    const changePage = (event) => {
        setPage(event.target.value)
    }

    useEffect(getUsers, [page])

    if (errorMessage) {
        return <ErrorMessage>{errorMessage}</ErrorMessage>
    }

    if (loading){
        return <Loading />
    }

    return (
        <>
            <Navbar />
            <CardContainer>
                {data.data && data.data.map((user) =>
                    <Card
                        key={user.id}
                        image={user.avatar}
                        title={user.first_name + " " + user.last_name}
                        text={user.email}
                    />
                )}
            </CardContainer>
            <Pagination onClick={changePage} howManyPages={data.total_pages} actualPage={page}/>
        </>
    );
}
