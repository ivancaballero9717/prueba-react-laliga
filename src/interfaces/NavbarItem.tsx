export interface NavbarItem {
    text: string;
    key: string;

    action()
}
