export interface CardProps{
    title:string;
    text:string;
    image?:string;
}
