export interface AuthState{
    token: string,
    errorMessage: string,
    loading: boolean
}
