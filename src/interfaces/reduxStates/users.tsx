import { UserResponse } from "../UserResponse";

export interface UsersState {
    data: UserResponse
    errorMessage: string;
    loading: boolean;
}
