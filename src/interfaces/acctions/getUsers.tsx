import { Action } from "./action";

export interface GetUsersAction extends Action {
    payload: number
}
