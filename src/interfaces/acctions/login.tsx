import { Action } from "./action";

export interface LoginAction extends Action{
    payload: LoginBody
}

export interface LoginBody {
    email: string;
    password: string;
}
