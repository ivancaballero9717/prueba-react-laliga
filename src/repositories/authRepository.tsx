import { api } from "../assets/settings";
import { checkStatusCode } from "../assets/utils";
import { LoginBody } from "../interfaces/acctions/login";

export const loginCall = (body: LoginBody) => {
    return fetch(api + "login/", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(response => {
        checkStatusCode(response);
        return response.json();
    })
}
