import { api } from "../assets/settings";
import { checkStatusCode } from "../assets/utils";

export const GetUsersCall = (page: number) => {
    return fetch(api + "users?page=" + page).then(response => {
        checkStatusCode(response);
        return response.json();
    })
}
