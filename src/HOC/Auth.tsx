import { useAuth } from "../hooks/useAuth";
import React from "react";
import { Redirect } from "react-router-dom";

export const withAuth = () => Component => props => {
    const { token } = useAuth();

    return (
        token ? (
            <Component {...props} />
        ) : (
            <Redirect to={"/"}/>
        )
    );
};
