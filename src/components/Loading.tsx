import React from "react";
import { Spinner } from "./Spinner";
import { StyledLoading } from "../styles/StyledLoading";

export const Loading = () => {
    return (
        <StyledLoading>
            <Spinner />
        </StyledLoading>
    )
}
