import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import { Login } from "../pages/Login";
import { Main } from "../pages/Main";
import { withAuth } from "../HOC/Auth";

import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
`;
  
const Root = () => (
    <BrowserRouter>
        <GlobalStyle />
        <div>
            <Route exact path="/" component={Login}/>
            <Route exact path="/main" component={withAuth()(Main)}/>
        </div>
    </BrowserRouter>
)

export default Root;
