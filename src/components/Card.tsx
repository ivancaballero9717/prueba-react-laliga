import React from "react";
import { StyledCard, StyledContent, StyledImage, StyledText, StyledTitle } from "../styles/CardStyles";
import { CardProps } from "../interfaces/props/CardProps";

export const Card = (props: CardProps) => {
    return (
        <StyledCard>
            <StyledContent>
                {props.image && <StyledImage src={props.image}/>}
                <StyledTitle>{props.title}</StyledTitle>
                <StyledText>{props.text}</StyledText>
            </StyledContent>
        </StyledCard>
    )
}
