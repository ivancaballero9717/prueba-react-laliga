import React from "react";
import { PaginationUlStyled, StyledPaginationItem } from "../styles/PaginationStyles";

interface PaginationProps {
    howManyPages: number;
    actualPage: number;

    onClick(event: any): void;
}

export const Pagination = (props: PaginationProps) => {

    let pages: any = []

    for (let page = 1; page <= props.howManyPages; page++) {
        const allowedProps = { active: props.actualPage === page }
        pages = [...pages,
            <StyledPaginationItem
                onClick={props.onClick}
                value={page}
                {...allowedProps}
                key={page}
            >
                {page}
            </StyledPaginationItem>
        ]
    }

    return (
        <PaginationUlStyled>
            {pages}
        </PaginationUlStyled>
    );
}
