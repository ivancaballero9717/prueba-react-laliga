import React from "react";
import { StyledCardContanier } from "../styles/CardStyles";

export const CardContainer = (props) => {
    return (<StyledCardContanier>{props.children}</StyledCardContanier>);
}
