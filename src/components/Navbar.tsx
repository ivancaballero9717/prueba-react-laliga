import React from "react";
import { StyledListContainer, StyledListItem, StyledNavbar } from "../styles/NavbarStyles";
import { NavbarItem } from "../interfaces/NavbarItem";
import { useDispatch } from "react-redux";
import { logout } from "../store/actions/auth";
import { useHistory } from "react-router-dom";

export const Navbar = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const navbarItems: NavbarItem[] = [
        {
            key: "logout",
            text: "Logout",
            action: () => {
                dispatch(logout());
                history.push("/")
            }
        }
    ]
    return (
        <StyledNavbar>
            <StyledListContainer>
                {navbarItems && navbarItems.map(item =>
                    <StyledListItem
                        key={item.key}
                        onClick={item.action}>{item.text}
                    </StyledListItem>)}
            </StyledListContainer>
        </StyledNavbar>
    )
}
