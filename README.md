# Prueba técnica React de LaLiga
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Versión de Node: 12.16.1 o superior
Versión de NPM: 6.13.4 o superiror
Compatibilidad: ES6 Navegadores evergreen (Chrome, Firefox, Edge, Safari)

## Instrucciones
- [Instrucciones](src/docs/laliga-prueba-tecnica-instrucciones.md)

## Entorno de desarrollo local

### `npm install`
Para instalación de dependencias

### `npm start`
Entorno de desarrollo

## Memoria

*** Puedes documentar aquí la memoria de tu prueba ***

La única libreria añadida ha sido redux-persist con el fin de mantener el token a pesar de que la ventana se cerrara.
También se podría haber hecho con cookies o localStorage pero me ha parecido mas interesante de cara a la prueba añadir una libreria de 0 como esta.

Respecto al proyecto, en la carpeta src entrontrareis todo el material de la aplicación distribuido por carpetas para facilitar encontrar todos los archivos siguiendo todos los requisitos solicitados.
